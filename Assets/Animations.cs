using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animations : MonoBehaviour
{
    Animator attack;

    IEnumerator Start()
    {
        attack = GetComponent<Animator>();
        yield return new WaitForSeconds(3);
        attack.SetTrigger("Walk");
    }

    public void Set_aniamator_int(int anim_ID)
    {
        attack.SetInteger("Attack", anim_ID);
    }
}
