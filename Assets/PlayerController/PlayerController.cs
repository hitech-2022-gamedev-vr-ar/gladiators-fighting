using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(Animator), typeof(NavMeshAgent))]
public class PlayerController : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator animator;
    float time = 0;

    void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 4;
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("IsAttacking");
        }
        
        TryRun();
    }


    private void TryRun()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            animator.SetBool("IsRunningForward", true);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            animator.SetBool("IsRunningForward", false);
        }
        if (Input.GetKeyDown(KeyCode.S) && Input.GetKeyDown(KeyCode.D))
        {
            animator.SetBool("IsRunningBR", true);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            animator.SetBool("IsRunningBack", true);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            animator.SetBool("IsRunningBack", false);
        }
        if (Input.GetKey(KeyCode.D))
            transform.rotation *= Quaternion.Euler(0f, 70f * Time.deltaTime, 0f);
        if (Input.GetKey(KeyCode.A))
            transform.rotation *= Quaternion.Euler(0f, -70f * Time.deltaTime, 0f);
    }
}
