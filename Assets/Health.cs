using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int health;
    public int maxHealth;

    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();   
    }

    public void TakeHit(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            animator.SetBool("IsDeathing", true);
            animator.SetBool("IsAttacking", false);
        }
    }
}
