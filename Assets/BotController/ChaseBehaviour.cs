using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator), typeof(NavMeshAgent))]
public class ChaseBehaviour : MonoBehaviour
{
    private Animator animator;
    NavMeshAgent agent;
    Transform player;
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        agent.speed = 4;

        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void Update()
    {
        float distance = Vector3.Distance(animator.transform.position, player.position);
        if(distance < agent.stoppingDistance)
        {
            animator.SetBool("IsAttacking", true);
        }
        else
        {
            agent.SetDestination(player.position);
            animator.SetBool("IsAttacking", false);
        }
    }
}
